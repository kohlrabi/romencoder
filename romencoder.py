#!/usr/bin/env python3
"""
Roman numeral encoder and decoder

Makes use of the fact that in Python 3.7+, dicts are guaranteed to be ordered. If version < 3.7, use OrderedDict
"""

# maps roman numeral glyphs to base10 number
_ROME2INT = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}


def encode(x: int) -> str:
    """
    Encodes an integer x to a roman numeral

    The integer x has be to larger than 0 and smaller than 4000

    :param x: integer x
    :type x: int
    :raises ValueError: Integer x out of range
    :return: roman numeral
    :rtype: str
    """
    # stores the resulting roman numeral string
    r = ''

    # generate a list of all numbers and glyphs in reversed order
    glyphs, nums = zip(*_ROME2INT.items())

    # check if the input integer is in the valid range
    if x < 1 or x > 3999:
        raise ValueError(
            f'Input number has to be larger than 0, and smaller than 4000, got {x}')

    # generate the numeral, as long as x is larger than zero
    start = len(nums) - 1
    while x:
        # shortcut for x < 4
        if x < 4:
            r += glyphs[0] * x
            break
        # iterate over the numbers from 1000 to 1
        for i in range(start, -1, -1):
            # if x is larger or equal to the number
            num = nums[i]
            if x >= num:
                # we add the glyph corresponding to the number
                r += glyphs[i]
                # then we subtract the number from x
                x -= num
                # finally we break the inner loop and continue the while loop
                break
            # we calculate the index of the valid prefix glyph for our current glyph
            # e.g. V has the prefix I, X has prefix I, L has prefix X, etc.
            j = ((i - 1) // 2) * 2
            # check if x is greater or equal to the current number minus the prefix
            num = nums[i] - nums[j]
            if x >= num:
                # then the add the prefix glyph and the glyph
                r += glyphs[j] + glyphs[i]
                # and subtract that number from x
                x -= num
                # finally we break out of the inner loop, and continue the while loop
                # the break is necessary to keep the current value of start
                break
            # skip the number we reached in future iterations
            start = i

    # return the resulting string
    return r


def decode(s: str) -> int:
    """
    Decodes a roman numeral string to an int

    :param s: roman numeral string
    :type s: str
    :return: base10 integer
    :rtype: int
    """
    # stores the resulting integer value
    r = 0

    # stores the int value of the last seen numeral, start with 1000 (== 'M')
    last = list(_ROME2INT.values())[-1]

    # iterate all the characters
    for c in s:
        # fetch the value of the character, this will raise KeyError if the roman numeral glyph is invalid
        v = _ROME2INT[c]
        # check whether the integer is larger than the last, i.e. we got a smaller prefix (4, 9, etc.)
        if v > last:
            # to 'correct' the prefix, we just subtract it twice, because we have already added it once and need to subtract it once
            r -= 2 * last
        # the new last seen value
        last = v
        # increment the result
        r += last

    # return the resulting integer
    return r


def test():
    """
    Testjig to test the encoder and decoder
    """
    # the valid range is 1 to 3999 inclusive
    for i in range(1, 4000):
        # encode the number
        e = encode(i)
        # decode the resulting numeral
        d = decode(e)
        # check whether we get our input number back
        assert i == d
        # show the result on screen
        print(i, e, d)


if __name__ == '__main__':
    """
    If run from the commandline, perform the test
    """
    test()
