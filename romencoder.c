#include <assert.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>

#define NUM 7
#define MAX_GLYPHS 20

const char glyphs[7] = "IVXLCDM";
const int nums[7] = {1, 5, 10, 50, 100, 500, 1000};

/*
 * encodes an unsigned integer to a roman numeral string
 * returns a pointer to a char buffer allocated on the heap
 * the user has to ensure that the buffer is free'd after use
 */
char *encode(unsigned int x) {
  int i, j, start = NUM - 1, ri = 0;

  char *r = malloc(sizeof(char) * MAX_GLYPHS);

  int num;

  if (x < 1 || x > 3999) {
    // error
    free(r);
    return 0;
  }

  while (x) {
    // shortcut for x < 4
    if (x < 4) {
      for (i = 0; i < x; i++) {
        r[ri++] = glyphs[0];  // I
      }
      break;
    }
    for (i = start; i >= 0; i--) {
      num = nums[i];
      if (x >= num) {
        r[ri++] = glyphs[i];
        x -= num;
        break;
      }
      j = ((i - 1) / 2) * 2;
      num = nums[i] - nums[j];
      if (x >= num) {
        r[ri++] = glyphs[j];
        r[ri++] = glyphs[i];
        x -= num;
        break;
      }
      start = i;
    }
  }
  r[ri++] = '\0';
  r = realloc(r, ri * sizeof(char));
  return r;
}

/*
 * decodes a roman numeral string to an unsigned integer
 */
unsigned int decode(const char *roman) {
  int index;
  unsigned int v, last = nums[6], r = 0;
  char c, *p;

  while ((c = *(roman++))) {
    p = strchr(glyphs, c);
    if (!p) {
      // error, character not found
      return 0;
    }
    index = (p - glyphs) / sizeof(char);
    v = nums[index];
    if (v > last) {
      r -= 2 * last;
    }
    last = v;
    r += last;
  }
  return r;
}

/*
 * Test decoder and encoder
 */
int main(int argc, char **argv) {
  int i, num;
  char *roman;

  for (i = 1; i < 4000; i++) {
    roman = encode(i);
    if (!roman) {
      // error, buffer already freed
      return 1;
    }
    num = decode(roman);
    if (!num) {
      // error, roman numeral contains invalid chars
      return 1;
    }
    assert(i == num);
    printf("%d %s %d\n", i, roman, num);
  }

  free(roman);
  return 0;
}